import {See} from '@serenity-js/core/lib/screenplay';
import {
    hasDadJoke,
    hasDadJokeWithId,
    ResponseMessage,
} from '../../acceptance_test/icanhasdadjokes/questions/response_message';
import {AsksForADadJoke, AsksForARandomDadJoke} from '../../acceptance_test/icanhasdadjokes/tasks/ask_for_dad_joke';

export = function deliverFundSteps() {

    let jokeId;

    this.Given(/random dad jokes$/, function() {
        return this.stage.theActorCalled('Admin').attemptsTo(
            // normally some data setup for random jokes
        );
    });

    this.When(/(.*) asks for a dad joke$/, function(actorName) {
        return this.stage.theActorCalled(actorName).attemptsTo(
            AsksForARandomDadJoke(),
        );
    });

    this.Then(/s?he should be told a dad joke$/, function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            See.if(ResponseMessage(), hasDadJoke()),
        );
    });

    this.Given(/a specific dad joke$/, function() {
        jokeId = 'R7UfaahVfFd';
        return this.stage.theActorCalled('Admin').attemptsTo(
            // normally some data setup for random jokes
        );
    });

    this.When(/(.*) asks for that dad joke$/, function(actorName) {
        return this.stage.theActorCalled(actorName).attemptsTo(
            AsksForADadJoke(jokeId),
        );
    });

    this.Then(/s?he should be told that dad joke$/, function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            See.if(ResponseMessage(), hasDadJoke()),
            See.if(ResponseMessage(), hasDadJokeWithId(jokeId)),
        );
    });
};
